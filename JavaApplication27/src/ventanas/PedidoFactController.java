/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ventanas;

import ValorInsuficienteException.ValorInsuficienteException;
import clasesRecursos.Cliente;
import clasesRecursos.menu;
import clasesRecursos.orden;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pedido.pedido;
import static ventanas.OpcionController.name;

/**
 * FXML Controller class
 *
 * @author Natanael
 */
public class PedidoFactController implements Initializable {

    private pedido pd;
    private StackPane delGrid;
    private HBox PedOpc;
    private VBox seccTotales;
    @FXML
    private ObservableList<orden> ordenes = FXCollections.observableArrayList();
    ;
    @FXML
    private TableColumn<orden, String> descripcion;
    @FXML
    private TableColumn<orden, String> cantidad;
    @FXML
    TableColumn<orden, String> valor;

    /**
     *
     */
    public static String name;
    private Button agregar;
    private double Dsubtotal;
    private double Diva;
    private double Dtotal;
    private VBox root;
    private ArrayList<String> descrip;
    private ArrayList<Integer> unidades;
    private Cliente user;
    private Text t1;
    private Text t2;
    private ArrayList<menu> t;
    @FXML
    private GridPane opc;
    @FXML
    private TableView<orden> ped;
    @FXML
    private ComboBox comboBoxMenu;
    @FXML
    private ComboBox comboBoxOrdenar;
    @FXML
    private Button cont;
    @FXML
    private Button limp;
    @FXML
    private Label sub;
    @FXML
    private Label iva;
    @FXML
    private Label tot;

    /**
     * Initializes the controller class.
     *
     * @param lista lista
     * @param descri descripcion
     * @param cant cantidad
     * @param precio precio
     */
    public void añadirTabla(ObservableList<orden> lista, String descri, int cant, double precio) {
        descripcion.setCellValueFactory(new PropertyValueFactory<orden, String>("descripcion"));

        cantidad.setCellValueFactory(new PropertyValueFactory<orden, String>("cantidad"));
        valor.setCellValueFactory(new PropertyValueFactory<orden, String>("valor"));

        double val = cant * precio;
        lista.add(new orden(descri, cant, val));
        ped.setItems(lista);

    }

    //METODO QUE PERMITE ACTUALIZAR LA SECCION DE OPCIONES SI SE CAMBIA LA FORMA DE ORDENAMIENTO
    /**
     *
     */
    public void actualizarOPC() {
        //ped = new TableView<orden>();
        /* Text descripcion = new Text("Descripción");
        Text precio = new Text("Precio");
        Text cantidad = new Text("Cantidad");
        descripcion.setStyle("-fx-font-weight:bolder;");
        cantidad.setStyle("-fx-font-weight:bolder;");
        precio.setStyle("-fx-font-weight:bolder;");*/

        switch (comboBoxMenu.getValue().toString()) {

            case ("Plato Fuerte"):
                String tipo = "F";
                añadirEvent(tipo);
                break;

            case ("Postre"):
                String tipo2 = "P";
                añadirEvent(tipo2);

                break;
            case ("Piqueo"):
                String tipo3 = "Q";
                añadirEvent(tipo3);
                break;

            case ("Bebida"):
                String tipo4 = "B";
                añadirEvent(tipo4);
                break;
        }
    }

    /**
     * Muestra el Menu
     * @param comboBoxMenu Menu
     */
    public void crearComboMenu(ComboBox comboBoxMenu) {
        comboBoxMenu.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                comboBoxOrdenar.setDisable(false);
                if (comboBoxOrdenar.getItems().isEmpty()) {
                    comboBoxOrdenar.getItems().addAll("Por precio", "Por nombre");
                }

                switch (comboBoxMenu.getValue().toString()) {

                    case ("Plato Fuerte"):
                        String tipo = "F";
                        añadirEvent(tipo);
                        break;

                    case ("Postre"):
                        String tipo2 = "P";
                        añadirEvent(tipo2);

                        break;
                    case ("Piqueo"):
                        String tipo3 = "Q";
                        añadirEvent(tipo3);
                        break;

                    case ("Bebida"):
                        String tipo4 = "B";
                        añadirEvent(tipo4);
                        break;
                }

            }

        });

    }

    
    /**
     * IMPLEMENTACION DE LOS EVENT HANDLER DE LOS BOTONES APLICAR
     * @param tipo Platillo
     */
    public void añadirEvent(String tipo) {
        opc.getChildren().clear();
        int i = 1;
        for (menu plato : t) {
            if (plato.getTipo().equals(tipo)) {

                TextField cant = new TextField();
                agregar = new Button("Agregar");
                agregar.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {

                        try {
                            if (cant.getText().equals("0") || cant.getText().equals(null)) {

                                validarCantidad();

                            } else {
                                descrip = new ArrayList();
                                unidades = new ArrayList();

                                descrip.add(plato.getDescripcion());

                                unidades.add(Integer.valueOf(cant.getText()));

                                añadirTabla(ordenes, plato.getDescripcion(), Integer.valueOf(cant.getText()), plato.getPrecio());
                                actValores();
                            }
                        } catch (NumberFormatException e) {
                            validarCantidad();
                        }

                    }

                });
                Label t1 = new Label(plato.getDescripcion());

                t1.setStyle("-fx-text-fill: white;");
                Label t2 = new Label(String.valueOf(plato.getPrecio()));
                t2.setStyle("-fx-text-fill: white;");

                opc.add(t1, 0, i);
                opc.add(t2, 1, i);
                opc.add(cant, 2, i);
                opc.add(agregar, 3, i);
                opc.setHgap(28);
                opc.setVgap(2);

                i++;
            }
        }

    }

    //CREA LOS COMBOXES E IMPLEMENTA SUS EVENTHANDLE
    /**
     *
     */
    public void crearCombo() {

        //comboBoxMenu=new ComboBox();
        comboBoxMenu.getItems().addAll("Plato Fuerte", "Postre", "Bebida", "Piqueo");
        // comboBoxOrdenar=new ComboBox();
        t = menu.cargarMenu();

        Collections.sort(t);

        comboBoxOrdenar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    if (comboBoxOrdenar.getValue().equals("Por nombre")) {
                        t = menu.cargarMenu("Por nombre");
                        Collections.sort(t);
                        opc.getChildren().clear();
                        actualizarOPC();

                    } else {
                        t = menu.cargarMenu();
                        Collections.sort(t);
                        opc.getChildren().clear();
                        crearComboMenu(comboBoxMenu);
                        actualizarOPC();
                    }
                } catch (NullPointerException e) {

                }

            }
        });

        crearComboMenu(comboBoxMenu);

    }
    //CREA LA SECCION INTERMEDIA QUE CONTIENE LA PARTE DE OPCIONES Y TABLA DE PEDIDOS CON TODAS SUS IMPLEMENTACIONES

    /*public void ordenarOpcion(String orden,String tipo){
      
       Opc=new VBox();
       PedOpc=new HBox();
       Opc.setSpacing(20);
       t=menu.cargarMenu(orden);
       Collections.sort(t);
       for(menu m: t){
           if(m.getTipo().equals(tipo)){
               PedOpc.getChildren().addAll(new Text(m.getDescripcion()),new Text((""+m.getPrecio()+"")),new Button("Agregar"));
               Opc.getChildren().addAll(PedOpc);
           }
       }
       Opc.setVisible(true);
      
    
    } */
    //METODO QUE ACTUALIZA LOS VALORES DE TOTAL SUBTOTAL E IVA CADA VEZ QUE SE AÑADE UN NUEVO PLATILLO
    /**
     *
     */
    public void actValores() {
        Dsubtotal = 0;
        Diva = 0;
        Dtotal = 0;
        for (orden ordenes : ordenes) {
            Dsubtotal += ordenes.getValor();
            Diva += ordenes.getValor() * 0.12;
            Dtotal = Dsubtotal + Diva;
            sub.setText(String.valueOf(Dsubtotal));
            iva.setText(String.valueOf(Diva));
            tot.setText(String.valueOf(Dtotal));

        }

    }

    //CREACION INICIAL DE LA SECCION QUE CONTIENE LOS LEMENTOS QUE MUESTRAN TOTAL SUBTOTAL E IVA, INICIALMENTE ESTARAN EN 0
    /**
     *
     */
    public void validarCantidad() {
        try {
            throw new ValorInsuficienteException();
        } catch (ValorInsuficienteException ex) {
            Alert a = new Alert(AlertType.WARNING);
            a.setTitle("");
            a.setContentText(ex.getMessage());
            a.showAndWait();

           

        }

    }

//Metodo que valida que no se pueda continuarsi no se ha hecho algun pedido
    /**
     *
     */
    public void existeCantidad() {
        Alert a = new Alert(AlertType.WARNING);
        a.setTitle("");
        a.setContentText("Ingrese una cantidad valida!");
        a.showAndWait();

    }

    /** 
     * Muestra la siguiente Ventana
     * @param nombre nombre
     * @return scene scene
     * @throws IOException Exception
     */
    public Scene Mostrar(String nombre) throws IOException {

        this.name = nombre;

        FXMLLoader loader = new FXMLLoader(getClass().getResource("PedidoFact.fxml"));
        Parent root2 = loader.load();
        Scene scene2 = new Scene(root2);

        return scene2;
    }

    /**
     * initialize
     * @param url UR
     * @param rb ResourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        sub.setText("0.00");
        iva.setText("0.00");
        tot.setText("0.00");
        cont.setOnMouseEntered(e -> cont.setStyle("-fx-background-color: red; -fx-text-fill: white;"));
        cont.setOnMouseExited(e -> cont.setStyle("-fx-background-color: orange; -fx-text-fill: black;"));
        limp.setOnMouseEntered(e -> limp.setStyle("-fx-background-color: red; -fx-text-fill: white; "));
        limp.setOnMouseExited(e -> limp.setStyle("-fx-background-color: orange; -fx-text-fill: black;"));
        cont.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {

                if (Double.valueOf(tot.getText()) != 0) {
                    Random ra = new Random();
                    int id = ra.nextInt(9999);
                    pd = new pedido(String.valueOf(id), descrip, unidades, name, null, Double.valueOf(sub.getText()), Double.valueOf(iva.getText()), Double.valueOf(tot.getText()));
                    pd.escribirFichero("\n" + pd.getIdpedido() + "," + pd.getCliente() + "," + pd.getTotal());
                    try {
                        ObjectOutputStream serializable = new ObjectOutputStream(new FileOutputStream("src/serial/pedido" + pd.getIdpedido() + ".bin"));
                        serializable.writeObject(pd);
                        serializable.close();

                        ((Node) (e.getSource())).getScene().getWindow().hide();
                        TransaccionController op = new TransaccionController();
                        Stage s = new Stage();
                        s.initModality(Modality.APPLICATION_MODAL);
                        s.setScene(op.Mostrar(pd));
                        s.setMaxWidth(630);
                        s.setMaxHeight(500);
                        s.show();

                    } catch (IOException ex) {
                        Logger.getLogger(OpcionController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                } else {
                    existeCantidad();

                }
            }
        }
        );
        limp.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                opc.getChildren().clear();
                ordenes.clear();
                try {

                    descrip.clear();
                    unidades.clear();

                } catch (NullPointerException e) {

                }

                Dsubtotal = 0;
                Diva = 0;
                Dtotal = 0;
                sub.setText("0.00");
                iva.setText("0.00");
                tot.setText("0.00");

            }

        });
        /*Opciones();
    totalPagar();
    botones();*/
        comboBoxOrdenar.setDisable(
                true);
        //comboBoxMenu.getItems().addAll("Plato Fuerte", "Postre", "Bebida", "Piqueo");
        //comboBoxOrdenar.getItems().addAll("Por precio", "Por nombre");
        crearCombo();

        /* comboBoxMenu.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                comboBoxOrdenar.setDisable(false);
                comboBoxOrdenar.setValue("Por nombre");
                switch (comboBoxMenu.getValue().toString()) {
                    
                    case ("Plato Fuerte"):
                        String tipo = "F";
                        comboBoxOrdenar.setOnAction(ev->ordenarOpcion((String) comboBoxOrdenar.getValue(),tipo));
                        
                        break;
                        
                    case ("Postre"):
                        String tipo2 = "P";
                        comboBoxOrdenar.setOnAction(ev->ordenarOpcion((String) comboBoxOrdenar.getValue(),tipo2));
                        
                        break;
                    case ("Piqueo"):
                        String tipo3 = "Q";
                        comboBoxOrdenar.setOnAction(ev->ordenarOpcion((String) comboBoxOrdenar.getValue(),tipo3));
                        
                        break;
                        
                    case ("Bebida"):
                        String tipo4 = "B";
                        comboBoxOrdenar.setOnAction(ev->ordenarOpcion((String) comboBoxOrdenar.getValue(),tipo4));
                        
                        break;
                }
            }
        });
        


        // TODO
    }*/
    }
}
