/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ventanas;

import clasesRecursos.pagoGenerado;
import java.io.IOException;
import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pedido.pedido;

/**
 * FXML Controller class
 *
 * @author Natanael
 */
public class TransaccionController implements Initializable {

    @FXML
    private Label sms;
    @FXML
    private TextField dir;
    @FXML
    private TextField ti;
    @FXML
    private TextField cad;
    @FXML
    private TextField cv;
    @FXML
    private TextField num;
    @FXML
    private RadioButton efec;
    @FXML
    private RadioButton tarj;
    @FXML
    private Button cont;
    @FXML
    private Button limp;
    @FXML
    private Pane t;
    private static pedido pd;

    /**
     * Initializes the controller class.
     */
    public TransaccionController() {

        this.pd = pd;

        // Text mensaje= new Text("Tendra que pagar un total de "+.getTotal()+"\nAsegurece de tener dinero completo");
    }

    /**
     * Muestra la siguiente ventana
     * @param pdi Pedido
     * @return scene scene
     * @throws IOException Execption
     */
    public Scene Mostrar(pedido pdi) throws IOException {

        this.pd = pdi;

        FXMLLoader loader = new FXMLLoader(getClass().getResource("Transaccion.fxml"));
        Parent root2 = loader.load();
        Scene scene2 = new Scene(root2);

        return scene2;
    }

    /**
     *
     */
    public void Mensaje() {
        sms.setText("Tendra que pagar un total de " + pd.getTotal() + "\nAsegurece de tener dinero completo");
    }

    /**
     * initialize
     * @param url URL
     * @param rb ResourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        efec.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                sms.setText(null);
                sms.setAlignment(Pos.CENTER);
                sms.setText("Tendra que pagar un total de " + pd.getTotal() + ".\nAsegurece de tener dinero completo");
                t.setVisible(false);

            }
        });

        t.setVisible(false);
        cont.setOnMouseEntered(e -> cont.setStyle("-fx-background-color: red; -fx-text-fill: white;"));
        cont.setOnMouseExited(e -> cont.setStyle("-fx-background-color: orange; -fx-text-fill: black;"));
        limp.setOnMouseEntered(e -> limp.setStyle("-fx-background-color: red; -fx-text-fill: white; "));
        limp.setOnMouseExited(e -> limp.setStyle("-fx-background-color: orange; -fx-text-fill: black;"));
        final ToggleGroup group = new ToggleGroup();
        efec.setToggleGroup(group);
        tarj.setToggleGroup(group);
        tarj.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                t.setVisible(true);
                sms.setText(null);
                sms.setAlignment(Pos.CENTER);
                sms.setText("       Tendra que pagar un total de " + pd.getTotal() * 1.05 + "\nPor el incremento del 5% por usar la tarjeta");
            }
        });
        cont.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                Random id = new Random();
                int valorDado = id.nextInt(9999);
                if (efec.isSelected() || tarj.isSelected()) {
                    if (efec.isSelected()) {
                        if (dir.getText().equals("")) {
                            Alert aler = new Alert(Alert.AlertType.WARNING);
                            aler.setContentText("Ingrese su direccion!");
                            aler.setHeaderText("");
                            aler.showAndWait();
                        } else {

                            pagoGenerado pg = new pagoGenerado(String.valueOf(valorDado), pd.getIdpedido(), pd.getCliente(), pd.getTotal(), null, "E");
                           
                            pg.escribirFichero("\n" + pg.getIdpago() + "," + pg.getIdpedido() + "," + pd.getCliente() + "," + pg.getTotalPagar() + "," + pg.getFecha() + "," + pg.getTipo());
                            try {
                                ((Node) (event.getSource())).getScene().getWindow().hide();
                                
                                Stage s = new Stage();
                                FinalController op = new FinalController();
                                s.setScene(op.Mostrar(pg .getIdpedido(),s));
                                System.out.print(pg.getIdpedido().toString());
                                s.initModality(Modality.APPLICATION_MODAL);
                                s.setMaxWidth(650);
                                s.setMaxHeight(490);
                                s.show();

                            } catch (IOException ex) {
                                Logger.getLogger(OpcionController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }

                    }

                    if (tarj.isSelected()) {
                        if ("".equals(dir.getText()) || "".equals(ti.getText())
                                || "".equals(num.getText()) || "".equals(cv.getText())
                                || "".equals(cad.getText())) {
                            Alert aler = new Alert(Alert.AlertType.WARNING);
                            aler.setContentText("Ingrese todos sus datos!");
                            aler.setHeaderText("");
                            aler.showAndWait();
                        } else {

                            if ((dir.getText().equals("")) || dir.getText().trim().equals("")) {
                                Alert alert = new Alert(Alert.AlertType.WARNING);
                                alert.setContentText("Ingrese correctamente sus datos!");
                                alert.setHeaderText("");
                                alert.showAndWait();

                            } else {
                                pagoGenerado pg = new pagoGenerado(String.valueOf(valorDado), pd.getIdpedido(), pd.getCliente(), pd.getTotal() * 1.05, null, "T");

                                pg.escribirFichero("\n" + pg.getIdpago() + "," + pg.getIdpedido() + "," + pd.getCliente() + "," + pg.getTotalPagar() + "," + pg.getFecha() + "," + pg.getTipo());
                                try {
                                ((Node) (event.getSource())).getScene().getWindow().hide();
                                Stage s = new Stage();
                                FinalController op = new FinalController();
                                
                                s.setScene(op.Mostrar(pg.getIdpedido().toString(),s));
                                System.out.print(pg.getIdpedido().toString());
                                s.initModality(Modality.APPLICATION_MODAL);
                                s.setMaxWidth(650);
                                s.setMaxHeight(400);
                                s.show();

                            } catch (IOException ex) {
                                Logger.getLogger(OpcionController.class.getName()).log(Level.SEVERE, null, ex);
                            }

                            }
                        }
                    }/*else{
                Alert aler = new Alert(Alert.AlertType.WARNING);
                        aler.setContentText("Seleccione un metodo de pago!");
                        aler.setHeaderText("");
                        aler.showAndWait();}*/
                } else {
                    Alert aler = new Alert(Alert.AlertType.WARNING);
                    aler.setContentText("Seleccione un metodo de pago!");
                    aler.setHeaderText("");
                    aler.showAndWait();
                }
            }

        });
        limp.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                sms.setText(null);
                dir.clear();
                ti.clear();
                cad.clear();
                cv.clear();
                num.clear();

            }
        });

        // TODO
    }

}
