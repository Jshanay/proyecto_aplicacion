/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ventanas;

import clasesRecursos.Cliente;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Natanael
 */
public class OpcionController implements Initializable {

    /**
     *
     */
    

    /**
     *
     */
    public static String name;
    private Stage s;

    private Cliente user;
    @FXML
    private Button mapa;
    @FXML
    private Button pedido;

    /**
     *
     */
    @FXML
    public Label saludo;

    /**
     *
     */
    public OpcionController() {
        this.mapa = mapa;
        this.pedido = pedido;
        this.saludo = saludo;

    }

    /**
     * Initializes the controller class.
     * @param nombre nombre
     * @param user1 user
     * @return Scene Scene
     * @throws java.io.IOException Exception
     */
    public Scene Mostrar(String nombre, Cliente user1) throws IOException {

        this.user = user1;
        this.name = nombre;

        FXMLLoader loader = new FXMLLoader(getClass().getResource("Opcion.fxml"));
        Parent root2 = loader.load();
        Scene scene2 = new Scene(root2);
        return scene2;
    }

    /**
     * Initialize
     * @param url URL
     * @param rb ResourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        saludo.setText("Bienvenid@ " + name + "!");
        mapa.setOnMouseEntered(e -> mapa.setStyle("-fx-background-color: red; -fx-text-fill: white;"));
        mapa.setOnMouseExited(e -> mapa.setStyle("-fx-background-color: orange; -fx-text-fill: black;"));
        pedido.setOnMouseEntered(e -> pedido.setStyle("-fx-background-color: red; -fx-text-fill: white; "));
        pedido.setOnMouseExited(e -> pedido.setStyle("-fx-background-color: orange; -fx-text-fill: black;"));
        mapa.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {

                try {
                    Parent root = FXMLLoader.load(getClass().getResource("Mapa.fxml"));
                    Scene scene = new Scene(root);
                    Stage s2 = new Stage();
                    s2.setTitle("");
                    s2.setScene(scene);
                    s2.initModality(Modality.APPLICATION_MODAL);
                    s2.setMaxWidth(715);
                    s2.setMaxHeight(550);
                    s2.showAndWait();
                } catch (IOException ex) {
                    Logger.getLogger(OpcionController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        pedido.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                try {
                   // ((Node) (t.getSource())).getScene().getWindow().hide();
                    PedidoFactController op = new PedidoFactController();
                    Stage s = new Stage();
                    s.setScene(op.Mostrar(name));
                    s.initModality(Modality.APPLICATION_MODAL);
                    s.setMaxWidth(950);
                    s.setMaxHeight(580);
                    s.show();
                    
                } catch (IOException ex) {
                    Logger.getLogger(OpcionController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

        });

    }

}
