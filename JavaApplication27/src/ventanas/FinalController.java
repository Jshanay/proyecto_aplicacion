/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ventanas;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import pedido.pedido;

/**
 * FXML Controller class
 *
 * @author Natanael
 */
public class FinalController implements Initializable {
     private static pedido pd;
    @FXML private Label tm;
    @FXML private Label p;
    private static String ct;
    private static Stage s;

    /**
     *
     * @param pdi id del pedido
     * @param st Stage
     * @return scene2 scene
     * @throws IOException Exception 
     */
    public Scene Mostrar(String pdi,Stage st) throws IOException {
        this.s=st;
        this.ct = pdi;

        FXMLLoader loader = new FXMLLoader(getClass().getResource("Final.fxml"));
        Parent root2 = loader.load();
        Scene scene2 = new Scene(root2);

        return scene2;
    }

    /**
     *
     */
    public void actualizar(){
        
   for (int i = 5; i >0; i--) {
            String status = ("Se cerrara en "+i);
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    tm.setText(status);
                    
                }
            });
               try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                        
                    } 
            

    
   }  Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    s.close();
                   //System.exit(0);                 
                    
                   
                    
                }

      
            });
    
    }

    /**
     *
     */
    public void empezar() {
        Runnable task = () -> actualizar();
        Thread backgroundThread = new Thread(task);
        backgroundThread.setDaemon(true);
        backgroundThread.start();
        
        }

    /**
     *
     * @param url URL
     * @param rb ResourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        p.setAlignment(Pos.CENTER);
        p.setText("Su pedido Nro " +ct+" ha sido pagado y ahora empezaremos a prepararlo");
        empezar();
        
        // TODO
    }    
} 
