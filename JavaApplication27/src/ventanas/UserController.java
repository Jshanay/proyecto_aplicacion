/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ventanas;

import clasesRecursos.Archivo;
import clasesRecursos.Cliente;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Scanner;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


/**
 * FXML Controller class
 *
 * @author Natanael
 */
public class UserController implements Initializable {

    private Stage s;

    ArrayList<Cliente> users = Archivo.cargarUsuarios();
    @FXML
    private TextField tfuser;
    @FXML
    private PasswordField tfpwd;
    @FXML
    private Button ing;
    @FXML
    private Label err;

    /**
     *
     */
    public UserController() {

        this.tfuser = tfuser;
        this.tfpwd = tfpwd;
        this.ing = ing;
        this.err = err;

    }

    /**
     * Initializes the controller class.
     * @param e Exception
     * @throws java.io.IOException Exception
     */
    public void ActionOnButton(ActionEvent e) throws IOException {
        String us = tfuser.getText();
        String contra = tfpwd.getText();
        for (Cliente usuario : users) {

            if ((us.equals(usuario.getUser())) && (contra.equals(usuario.getContrasena()))) {
                ((Node) (e.getSource())).getScene().getWindow().hide();
                OpcionController op= new  OpcionController();
                Stage s = new Stage();
                s.setScene(op.Mostrar(usuario.getNickName(), usuario));
                s.setMaxWidth(640);
                s.setMaxHeight(424);
                s.show();
               

            } else {
                err.setVisible(true);
                err.setText("Usuario y/o contraseña incorrecta!");
                tfuser.clear();
                tfpwd.clear();
            }

        }
    }

    /**
     * Initialize
     * @param url URL
     * @param rb ResourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle rb
    ) {

        ing.setOnMouseEntered(e -> ing.setStyle("-fx-background-color: red; -fx-text-fill: white;"));
        ing.setOnMouseExited(e -> ing.setStyle("-fx-background-color: orange; -fx-text-fill: black;"));
        tfuser.setOnMouseClicked(e -> err.setText(null));
        tfpwd.setOnMouseClicked(e -> err.setText(null));

        // TODO
    }

}
