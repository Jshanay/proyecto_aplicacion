/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ventanas;

import clasesRecursos.Archivo;
import clasesRecursos.locales;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Natanael
 */
public class MapaController implements Initializable {

    private static ImageView imv;
    private static ImageView imv2;
    @FXML
    private Pane root;
    private Stage s;

//46 39
    /**
     * Initializes the controller class.
     * @param nombre nombre
     * @param direccion direccion
     * @param horario horario
     * @return imv2
     */
    public ImageView crearImagen(String nombre, String direccion, String horario) {

       imv2 = new ImageView();
        try {
            imv2.setImage(new Image(new FileInputStream("src/recursos/ubicacion.png")));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MapaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        imv2.setFitHeight(46);
        imv2.setFitWidth(39);
        imv2.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                popUp(nombre, direccion, horario);

            }
        });
        return imv2;

    }

    /**
     * Retorna una lista dee objetos local 
     * @return local Objeto local
     */
    public ArrayList<locales> leerFichero() {
        ArrayList<locales> local = new ArrayList();
        try {

            FileReader fr = new FileReader("src/recursos/locales.txt");
            BufferedReader br = new BufferedReader(fr);
            String linea;
            while (!((linea = br.readLine()) == null)) {
                String[] info = linea.split(",");
                local.add(new locales(Double.valueOf(info[0].trim()), Double.valueOf(info[1].trim()), (info[2].trim()), (info[3].trim()), (info[4].trim())));

            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return local;

    }

    /**
     *
     */
    public void actualizar() {

        ArrayList<locales> listLocales = leerFichero();
        for (locales local : listLocales) {

            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    ImageView icono = crearImagen(local.getNombre(), local.getDireccion(), local.getHorario());

                    icono.setLayoutX(local.getPosicioX());
                    icono.setLayoutY(local.getPosicioY());
                    root.getChildren().addAll(icono);

                }
            });
            try {
                int num = (int) (Math.random() * 10 + 1);
                System.out.println(num);
                Thread.sleep(num * 1000);
            } catch (InterruptedException ex) {
//                        
            }

        }
    }

    ;
    
    /**
     *
     */
    public void empezar() {
        Runnable task = () -> actualizar();
        Thread backgroundThread = new Thread(task);
        backgroundThread.setDaemon(true);
        backgroundThread.start();

    }
//color

    /**
     * metodo genera el PopUp en pantalla(Mapa)
     * @param nombre nombre
     * @param direccion direccion
     * @param horario Horario
     */
    public void popUp(String nombre, String direccion, String horario) {
        Rectangle rect = new Rectangle(300, 200);
        Button bt = new Button("Aceptar");
        bt.setStyle("-fx-background-color: orange; -fx-text-fill: black;");
        VBox vb = new VBox();
        HBox hb = new HBox();
        hb.setSpacing(80);
        Text nom = new Text(nombre);
        Text direc = new Text(direccion);
        Text hor = new Text(horario);
        nom.setStyle("-fx-font-size: 15px;-fx-font-weight:bolder;");
        direc.setStyle("-fx-font-size: 15px;-fx-font-weight:bolder;");
        hor.setStyle("-fx-font-size:15px;-fx-font-weight:bolder;");
        StackPane stack = new StackPane();
        Text text = new Text("");
        hb.getChildren().addAll(text, bt);
        hb.setAlignment(Pos.CENTER);
        rect.setFill(Color.ORANGE);
        vb.getChildren().addAll(nom, direc, hor, hb);
        vb.setAlignment(Pos.CENTER);
        vb.setSpacing(30);
        stack.getChildren().addAll(rect, vb);
        stack.setLayoutX(230);
        stack.setLayoutY(100);
        bt.setOnMouseEntered(e -> bt.setStyle("-fx-background-color: red; -fx-text-fill: white;"));
        bt.setOnMouseExited(e -> bt.setStyle("-fx-background-color: orange; -fx-text-fill: black;"));
        bt.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                stack.getChildren().clear();

            }

        });
        root.getChildren().addAll(stack);
        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 5; i > 0; i--) {

                    text.setText("Mostrando " + i+" segundos...");

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
//                       Logger.getLogger(Mapa.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        stack.getChildren().clear();

                    }
                });

            }

        });
        th.start();

    }

    ;
           
    /**
     * INITIALIZE
     * @param url URL
     * @param rb ResourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        imv = new ImageView();
        try {
            imv.setImage(new Image(new FileInputStream("src/recursos/mapa.png")));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MapaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        imv.setFitHeight(500);
        imv.setFitWidth(700);
        root.getChildren().addAll(imv);
        empezar();

        // TODO
    }

}
