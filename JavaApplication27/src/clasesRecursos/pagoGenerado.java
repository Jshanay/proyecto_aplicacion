/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clasesRecursos;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import pedido.pedido;

/**
 *
 * @author PC
 */
public class pagoGenerado {

    private String Idpago;
    private String idpedido;
    private String nombreCLiente;
    private double totalPagar;
    private String fecha;
    private String tipo;

    /**
     * Contructor de pagoGenerado
     *
     * @param Idpago pago
     * @param idpedido pedido
     * @param nombreCLiente nombre
     * @param totalPagar total
     * @param fecha fecha
     * @param tipo tipo platillo
     */
    public pagoGenerado(String Idpago, String idpedido, String nombreCLiente, double totalPagar, String fecha, String tipo) {
        this.Idpago = Idpago;
        this.idpedido = idpedido;
        this.nombreCLiente = nombreCLiente;
        this.totalPagar = totalPagar;
        this.fecha = fecha;
        this.tipo = tipo;
    }

    /**
     * Escribe los pagos que se generan en la factura
     *
     * @param pedido Pedido
     */
    public void escribirFichero(String pedido) {
        try {
            File file = new File("src/recursos/pagos.txt");
            FileWriter w = new FileWriter(file, true);
            BufferedWriter bf = new BufferedWriter(w);
            bf.write(pedido);
            bf.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }

    /**
     * setter
     *
     * @param Idpago id del pago
     */
    public void setIdpago(String Idpago) {
        this.Idpago = Idpago;
    }

    /**
     * setter
     *
     * @param idpedido id del pedido
     */
    public void setIdpedido(String idpedido) {
        this.idpedido = idpedido;
    }

    /**
     * setter
     *
     * @param nombreCLiente nombre del cliente
     */
    public void setNombreCLiente(String nombreCLiente) {
        this.nombreCLiente = nombreCLiente;
    }

    /**
     * setter
     *
     * @param totalPagar total de pago
     */
    public void setTotalPagar(double totalPagar) {
        this.totalPagar = totalPagar;
    }

    /**
     * setter
     *
     * @param fecha fecha
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    /**
     * setter
     *
     * @param tipo tipo
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * getter
     *
     * @return Idpago
     */
    public String getIdpago() {
        return Idpago;
    }

    /**
     * getter
     *
     * @return idpedido
     */
    public String getIdpedido() {
        return idpedido;
    }

    /**
     * getter
     *
     * @return nombreCLiente
     */
    public String getNombreCLiente() {
        return nombreCLiente;
    }

    /**
     * retorna el valor de total a pagar de la factura
     *
     * @return totalpagar
     */
    public double getTotalPagar() {
        return totalPagar;
    }

    /**
     * getter
     *
     * @return fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * getter
     *
     * @return tipo
     */
    public String getTipo() {
        return tipo;
    }
}
