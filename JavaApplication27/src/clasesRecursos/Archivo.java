/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clasesRecursos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Natanael
 */
public class Archivo {

    /**
     * Retorna un ArrayList de Clientes de la lectura del archivo de usuarios
     * @return usu
     */
    public static ArrayList<Cliente> cargarUsuarios() {
        ArrayList<Cliente> usu = new ArrayList<>();
        try (
                BufferedReader bf = new BufferedReader(new FileReader("src/recursos/usuario.txt"))) {
            String linea;
            bf.readLine();
            while ((linea = bf.readLine()) != null) {

                String datos[] = linea.split(",");

                usu.add(new Cliente(datos[0].trim().toString(), datos[1].trim().toString(), datos[2].trim().toString()));

            }

        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());

        }

        return usu;

    }

    

}
