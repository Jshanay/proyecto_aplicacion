/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clasesRecursos;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author PC
 */
public class Cliente {

    /**
     *La clase sirve para crear una instancia cliente
     */
    public String usuarioGlobal;
    private String user;
    private String contrasena;
    private String nickName;

    /**
     * Constructor de la Clase Cliente
     * @param user usuario
     * @param contrasena contraseña
     * @param nickName nombre
     */
    public Cliente(String user, String contrasena, String nickName) {
        this.user = user;
        this.contrasena = contrasena;
        this.nickName = nickName;
        
    }

   

    @Override
    public String toString() {
        return "Cliente{" + "user=" + user + ", contrasena=" + contrasena + ", nickName=" + nickName + '}';
    }
    
    /**
     * setter
     * @param user user 
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * setter
     * @param contrasena contraseña
     */
    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    /**
     * setter
     * @param nickName nickName
     */
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    /**
     * getter
     * @return user user
     */
    public String getUser() {
        return user;
    }

    /**
     * getter
     * @return contraseña contraseña
     */
    public String getContrasena() {
        return contrasena;
    }

    /**
     * getter
     * @return nickName nickName
     */
    public String getNickName() {
        return nickName;
    }
    
}
