/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clasesRecursos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author PC
 */
public class locales {
 private double posicioX;
 private double posicioY;
 private String nombre;
 private String direccion;        
 private String horario;   

    public locales(double posicioX, double posicioY, String nombre, String direccion, String horario) {
        this.posicioX = posicioX;
        this.posicioY = posicioY;
        this.nombre = nombre;
        this.direccion = direccion;
        this.horario = horario;
    }

    
    
    
    
    
    
    
    
    
    
    public double getPosicioX() {
        return posicioX;
    }

    public double getPosicioY() {
        return posicioY;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getHorario() {
        return horario;
    }

    public void setPosicioX(double posicioX) {
        this.posicioX = posicioX;
    }

    public void setPosicioY(double posicioY) {
        this.posicioY = posicioY;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }
    
}
