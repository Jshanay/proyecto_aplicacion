/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clasesRecursos;

/**
 *
 * @author PC
 */
public class orden {
    
    private String descripcion;
    private int cantidad;
    private double valor;

    /**
     * Constructor de la clase orden
     * @param descripcion descripcion
     * @param cantidad cantidad
     * @param valor valor
     */
    public orden(String descripcion, int cantidad, double valor) {
        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.valor = valor;
    }

    /**
     * setter
     * @param descripcion descripcion
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * setter
     * @param cantidad cantidad
     */
    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    /**
     * setter
     * @param valor valor
     */
    public void setValor(int valor) {
        this.valor = valor;
    }

    /**
     * getter
     * @return descripcion descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * getter 
     * @return cantidad cantidad
     */
    public int getCantidad() {
        return cantidad;
    }

    /**
     * getter
     * @return valor valor
     */
    public double getValor() {
        return valor;
    }
    
    
    
    
    
}
