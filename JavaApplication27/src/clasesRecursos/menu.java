/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clasesRecursos;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author PC
 */
public class menu implements Comparable<menu>  {
    private String descripcion;
    private double precio;
    private String tipo;
    private String ordenar;

    /**
     * Constructor de la clase menu.
     * @param descripcion descripcion    
     * @param precio precio  
     * @param tipo tipo 
     */
    public menu(String descripcion, double precio, String tipo) {
        this.descripcion = descripcion;
        this.precio = precio;
        this.tipo = tipo;
    }

    /**
     * Retorna una lisa ordenada de acuerdo al comando que se le de.
     * @return men
     */
    public static ArrayList<menu> cargarMenu(){
        ArrayList<menu> men=new ArrayList<>();
        try(
        BufferedReader bf=new BufferedReader(new FileReader("src/recursos/menu.txt"))){
        String linea; 
        bf.readLine();
        while((linea=bf.readLine())!=null){
            
            String datos[]=linea.split(",");
            
            menu menu=new menu(datos[0],(Double.valueOf(datos[1])),datos[2]);
            menu.setOrdenar("0");
            men.add(menu);
        
        }
        
        }catch(FileNotFoundException ex){
            System.out.println(ex.getMessage());
        }catch(IOException ex){
            System.out.println(ex.getMessage());
        
        }
        
        return men;
        
    }

    /**
     * Retorna una lisa ordenada de acuerdo al comando que se le de.
     * @param ordenar orden
     * @return menu menu
     */
    public static ArrayList<menu> cargarMenu(String ordenar){
        ArrayList<menu> men=new ArrayList<>();
        try(
        BufferedReader bf=new BufferedReader(new FileReader("src/recursos/menu.txt"))){
        String linea; 
        bf.readLine();
        while((linea=bf.readLine())!=null){
            
            String datos[]=linea.split(",");
            menu menu=new menu(datos[0],(Double.valueOf(datos[1])),datos[2]);
            menu.setOrdenar(ordenar);
            men.add(menu);
        
        }
        
        }catch(FileNotFoundException ex){
            System.out.println(ex.getMessage());
        }catch(IOException ex){
            System.out.println(ex.getMessage());
        
        }
        
        return men;
        
    }
    
    /**
     * setter
     * @param descripcion descripcion
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * setter
     * @param precio precio
     */
    public void setPrecio(double precio) {
        this.precio = precio;
    }

    /**
     * setter
     * @param tipo tipo
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * setter
     * @param ordenar ordenar
     */
    public void setOrdenar(String ordenar) {
        this.ordenar = ordenar;
    }

    /**
     * getter
     * @return descripcion descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * getter
     * @return precio precio
     */
    public double getPrecio() {
        return precio;
    }

    /**
     *getter
     * @return tipo tipo
     */
    public String getTipo() {
        return tipo;
    }
    
    public int compareTo(menu menues) {
       if(ordenar.equals("Por nombre")){
           return this.descripcion.compareToIgnoreCase(menues.descripcion);  
       }else{
         
          return String.valueOf(this.precio).compareTo(String.valueOf(menues.precio));
       }
    
    
    }}
