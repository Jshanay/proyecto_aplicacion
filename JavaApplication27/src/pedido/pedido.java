/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pedido;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author PC
 */
public class pedido implements Serializable {

    private ArrayList<String> comida;
    private ArrayList<Integer> cantidad;
    private String cliente;
    private String direccion;
    private double subtotal;
    private double iva;
    private double total;
    private String idpedido;

    /**
     * Constrictor de la clase pedido
     *
     * @param idpedido id del pedido
     * @param comida comida
     * @param cantidad cantidad
     * @param cliente Objeto cliente
     * @param direccion direccion
     * @param subtotal subtotal
     * @param iva iva
     * @param total total
     */
    public pedido(String idpedido, ArrayList<String> comida, ArrayList<Integer> cantidad, String cliente, String direccion, double subtotal, double iva, double total) {
        this.idpedido = idpedido;
        this.comida = comida;
        this.cantidad = cantidad;
        this.cliente = cliente;
        this.direccion = direccion;
        this.subtotal = subtotal;
        this.iva = iva;
        this.total = total;
    }

    /**
     * Escribe el archivo txt de pedidos
     *
     * @param pedido Objeto pedido
     */
    public void escribirFichero(String pedido) {
        try {

            FileWriter w = new FileWriter("src/recursos/pedidos.txt", true);
            BufferedWriter bf = new BufferedWriter(w);
            bf.write(pedido);
            bf.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * setter
     *
     * @param idpedido numpedido
     */
    public void setIdpedido(String idpedido) {
        this.idpedido = idpedido;
    }

    /**
     * setter
     *
     * @param subtotal subtotal
     */
    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    /**
     * setter
     *
     * @param iva iva
     */
    public void setIva(double iva) {
        this.iva = iva;
    }

    /**
     * setter
     *
     * @param total total
     */
    public void setTotal(double total) {
        this.total = total;
    }

    /**
     * setter
     *
     * @param comida comida
     */
    public void setComida(ArrayList<String> comida) {
        this.comida = comida;
    }

    /**
     * setter
     *
     * @param cantidad cantidad
     */
    public void setCantidad(ArrayList<Integer> cantidad) {
        this.cantidad = cantidad;
    }

    /**
     * setter
     *
     * @param cliente setter
     */
    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    /**
     * sette
     *
     * @param direccion setter
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * getter de array
     *
     * @return comida ArrayList de Comida
     */
    public ArrayList<String> getComida() {
        return comida;
    }

    /**
     * getter de array
     *
     * @return cantidad ArrayList int cantidad
     */
    public ArrayList<Integer> getCantidad() {
        return cantidad;
    }

    /**
     * getter
     *
     * @return cliente cliente
     */
    public String getCliente() {
        return cliente;
    }

    /**
     * getter
     *
     * @return direccion direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * getter
     *
     * @return subtotal subtotal
     */
    public double getSubtotal() {
        return subtotal;
    }

    /**
     * getter
     *
     * @return iva iva
     */
    public double getIva() {
        return iva;
    }

    /**
     * getter
     *
     * @return total total
     */
    public double getTotal() {
        return total;
    }

    /**
     * getter
     *
     * @return id pedido pedido
     */
    public String getIdpedido() {
        return idpedido;
    }

}
